import random
from flask import Flask, request

app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        return {"message": "get request", "random_number": random.randint(10000, 50000)}
    if request.method == "POST":
        return {
            "message": "post request",
            "random_number": random.randint(10000, 50000),
        }


@app.errorhandler(Exception)
def not_found(e):
    return {"message": "404 not found"}


if __name__ == "___main_":
    app.run(port=5050, host="0.0.0.0")
