# Pick any one of the below scenarios

## Prerequisites

You will need to have docker, docker-compose and python installed in your system for testing and completing this assignment. Access to an AWS environment for testing changes might be necessary.

Another engineer should be able to reproduce your solution in isolation by following your instructions on the README.

Document your work for each step of each assignment. Use small commits and document your steps as a README in each solution folder. Do not commit all of your work together as a single commit.

Create a **private** git repository **cloning** this repo. Do not fork!

Link a loom video explaining your approach and solution in the solution README(s)

When you are satisfied with your solution, add jojin@lokyata.com as a collaborator to your repository and reply to your assignment email.

Good luck and have fun!

## Assignment 1

1. Deploy a Flask-based api in a Docker container using an Nginx proxy in a separate container.
2. The flask application should not be served by the flask development server. Feel free to make changes to the flask container for this purpose.
3. The flask application should return a random number between 10000 and 50000 as the response for an http GET/POST request to the api.
4. Create a docker compose definition for this application for local testing.
5. Create a cloudformation template to deploy this application to AWS
6. Ensure that there is a feature to scale the number of Flask containers up and down with Nginx automatically proxying them
7. The application logs from all the containers of the application should be available on a single cloudwatch log group.

Bonus points:
For local testing, hook up the application to a postgres database and write the incoming requests to the database for storage.
Manage the database schema using flyway/liquibase. This should be part of the docker-compose file. This need not be deployed to AWS

## Assignment 2

1. Design a repo structure for the above app
2. Add documentation on how to use docker-compose for testing this app
3. Design a branching strategy for the app for a team of 20 developers
4. Design 3 different cloud architecture options to deploy this application on AWS. Provide architecture diagrams, deployment, monitoring and rollback options. Illustrate your approach through draw.io diagrams.
5. Create one or more Jenkinsfile(s) to build, test and deploy this app to AWS - add the jenkinsfiles, bash scripts for testing in the solutions folder.

## Assignment 3

Create a cloudformation template that demonstrates all of the following:

1. VPC
2. NAT and Internet gateway
3. Autoscaling group with a minimum of 2 webservers running apache server which scales up when RAM is above 70% or CPU usage is above 80%
4. Webservers connect to a postgres RDS/Aurora instance
5. Application load balancer in the public layer of the VPC
6. Webservers should be accessible only through SSM
7. Webservers must be in a private subnet
8. Database must be in it's own subnet that is accessible only from the webserver private subnet
9. Use cloud-init to add and enable apache webserver on the instances.

Document how high availability can be acheived for this architecture across all layers. This need not be implemented.

Bonus points:
Add automated patching for the webservers. All webservers should not be patched at once.
Add an ssm document to the template which lists the top 5 unique ip addresses accessing your servers and runs every day at 5pm
Add an ssm document to the template which lists the top 10 directories by disk-space usage - this can be an ad-hoc document


